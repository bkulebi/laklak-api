FROM python:3.5-slim
RUN apt-get update && \
    apt-get -y install gcc
RUN mkdir -p /opt/apps
ENV APP_DIR /opt/apps/laklak-api
ENV CELERY_BROKER_URL redis://redis
ADD . $APP_DIR
WORKDIR $APP_DIR
RUN pip install -r requirements.txt
ADD .docker/bin/* /usr/local/bin/
EXPOSE 5000
ENTRYPOINT ["entry"]
CMD ["app"]
