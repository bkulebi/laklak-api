import os
from laklak import create_app, db
from laklak.models import Segment, Collection, User
from flask_script import Manager, Command, Shell
from flask_migrate import Migrate, MigrateCommand

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app=app)
migrate = Migrate(app, db)

def make_shell_context():
    return dict(app=app,
                db=db,
                Segment=Segment,
                Collection=Collection,
                User=User)

manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command("db", MigrateCommand)

@manager.command
def test():
    """Run the unit tests."""
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)

@manager.command
def recreate_db():
    """Recreates a database."""
    db.drop_all()
    db.create_all()
    db.session.commit()

@manager.command
def create_test_db():
    """Recreates a database."""
    db.drop_all()
    db.create_all()

    user1 = User(username='mao',password='1935')
    user2 = User(username='roza', password='1921')
    db.session.add(user1)
    db.session.add(user2)
    db.session.commit()

if __name__ == '__main__':
    manager.run()
