import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from celery import Celery
from config import config

db = SQLAlchemy()
celery = Celery(__name__,
                broker=os.environ.get('CELERY_BROKER_URL', 'redis://'),
                backend=os.environ.get('CELERY_BROKER_URL', 'redis://'))

from . import models
#from .tasks import create_automated_segments # import for the task registry

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])

    db.init_app(app)

    from .api_1_0 import api as api_1_0_blueprint
    app.register_blueprint(api_1_0_blueprint, url_prefix='/laklak/1.0')

    from .api_1_0.token import token as token_blueprint
    app.register_blueprint(token_blueprint, url_prefix='/auth')

    from .tasks import tasks as tasks_blueprint
    app.register_blueprint(tasks_blueprint, url_prefix='/tasks')

    return app
