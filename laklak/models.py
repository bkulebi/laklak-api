from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import url_for, current_app
from . import db

class Segment(db.Model):
    __tablename__ = 'segments'
    id = db.Column(db.Integer, primary_key=True)
    start_seconds = db.Column(db.Float)
    end_seconds = db.Column(db.Float)
    transcript = db.Column(db.String(128)) # TODO check length
    confirmed = db.Column(db.Boolean)
    note = db.Column(db.String(32))

    collection_id = db.Column(db.Integer,db.ForeignKey('collections.id'))

    @staticmethod
    def from_json(json_segment):
        start = float(json_segment.get('start_seconds'))
        end = float(json_segment.get('end_seconds'))
        return Segment(start_seconds=start,
                       end_seconds=end,
                       confirmed=False,
                       transcript=json_segment.get('transcript'),
                       note=json_segment.get('note'))

    def to_json(self):
        if not self.collection_id:
            raise AttributeError('segment is orphaned: does not have' \
                                                             ' a collection_id')
        collection = Collection.query.filter_by(id=self.collection_id).first()
        json_segment = {'url': url_for('api.get_segment',
                                       collection_id=self.collection_id,
                                       segment_id=self.id,
                                       _external=True),
                        'id': self.id,
                        'start_seconds': self.start_seconds,
                        'end_seconds': self.end_seconds,
                        'collection_name': collection.collection_name,
                        'transcript': self.transcript,
                        'note': self.note,
                        'confirmed': self.confirmed}
        return json_segment

    def __repr__(self):
        return '<Segment {:3.2f}s:{:3.2f}s>'.format(self.start_seconds,
                                                  self.end_seconds)

class Collection(db.Model):
    __tablename__ = 'collections'
    id = db.Column(db.Integer, primary_key=True)
    collection_name = db.Column(db.String(64)) # TODO should be unique per user
    uri = db.Column(db.String(64))
    path = db.Column(db.String(64))
    segments = db.relationship('Segment',backref='collections',lazy='dynamic')
    download_method = db.Column(db.String(64))

    user_id = db.Column(db.Integer,db.ForeignKey('users.id'))

    @staticmethod
    def from_json(json_collection):
        collection_name = json_collection.get('collection_name')
        uri = json_collection.get('uri')
        download_method = json_collection.get('download_method')
        return Collection(collection_name=collection_name,
                          uri=uri,
                          download_method=download_method)

    def to_json(self):
        if not self.user_id:
            raise AttributeError('collection is orphaned: does not have a user_id')
        user = User.query.filter_by(id=self.user_id).first()
        json_collection = {'url': url_for('api.get_collection',
                                          collection_id=self.id,
                                          _external=True),
                           'id': self.id,
                           'collection_name': self.collection_name,
                           'username': user.username,
                           'uri': self.uri,
                           'download_method': self.download_method,
                           'path': self.path}
        return json_collection 

    def __repr__(self):
        return '<Collection %s>' % self.collection_name

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), unique=True)
    password_hash = db.Column(db.String(128))
    collections = db.relationship('Collection',backref='users',lazy='dynamic')

    @staticmethod
    def from_json(json_user):
        username = json_user.get('username')
        password = json_user.get('password')
        return User(username=username,
                    password=password)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_auth_token(self, expires_in=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expires_in=expires_in)
        return s.dumps({'id': self.id}).decode('utf-8')

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None
        return User.query.get(data['id'])

    def to_json(self):
        json_user = {'url': url_for('api.get_user',
                                    user_id=self.id,
                                    _external=True),
                     'username': self.username,
                     'id': self.id}
        return json_user

    def __repr__(self):
        return '<User %s>' % self.username
