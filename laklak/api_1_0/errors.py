from flask import make_response, jsonify, g
from . import api

@api.app_errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'not found'}), 404)

def get_collection_or_404(collection_id):
    current_collection = g.user.collections.filter_by(id=collection_id).first()
    if not current_collection:
        abort(404, 'collection not found (for the current user)')
    return current_collection

