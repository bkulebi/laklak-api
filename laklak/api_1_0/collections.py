from flask import jsonify, abort, request, url_for, g
from . import api
from .. import db
from ..models import Collection
from .errors import get_collection_or_404
from ..errors import forbidden
from ..tasks import download_collection_resource

@api.route('/collections', methods=['GET'])
def get_collections():
    collections = [collection.to_json() for collection in \
                                                      g.user.collections.all()]
    return jsonify({'collections':collections})

@api.route('/collections', methods=['POST'])
def create_collection():
    if not request.json or not 'collection_name' in request.json:
        abort(400)
    collection = Collection.from_json(request.json)
    collection.user_id = g.user.id
    db.session.add(collection)
    db.session.commit()
    return jsonify(collection.to_json()), 201, \
           {'Location': url_for('api.get_collection',
                                collection_id=collection.id,
                                _external=True)}

@api.route('/collections/<int:collection_id>', methods=['GET'])
def get_collection(collection_id):
    collection = Collection.query.get_or_404(collection_id)
    if collection.user_id != g.user.id:
        return forbidden("Insuffient permission")
    return jsonify(collection.to_json())

@api.route('/collections/<int:collection_id>', methods=['PUT'])
def update_collection(collection_id):
    collection = Collection.query.get_or_404(collection_id)
    if collection.user_id != g.user.id:
        return forbidden("Insuffient permission")
    if not request.json:
        abort(400)
    if 'collection_name' in request.json:
        if type(request.json['collection_name']) != str: #TODO check redundancy
            abort(400)
        else:
            collection.collection_name = request.json.get('collection_name',
                                                    collection.collection_name)
    if 'uri' in request.json:
        if type(request.json['uri']) != str: #TODO check if redundant
            abort(400)
        else:
            collection.uri = request.json.get('uri',collection.uri)
    db.session.commit()
    return jsonify(collection.to_json()), 201, \
           {'Location': url_for('api.get_collection',
                                collection_id=collection.id,
                                _external=True)}

@api.route('/collections/<int:collection_id>', methods=['DELETE'])
def delete_collection(collection_id):
    collection = Collection.query.get_or_404(collection_id)
    if collection.user_id != g.user.id:
        return forbidden("Insuffient permission")
    db.session.delete(collection)
    db.session.commit()
    return jsonify({'deleted':collection_id}), 201

@api.route('/collections/<int:collection_id>/create_collection_resource', methods=['POST'])
def create_collection_resource(collection_id):
    current_collection = get_collection_or_404(collection_id)
    if not current_collection.uri:
        #TODO log "collection uri does not exist"
        abort(400)
    task = download_collection_resource.apply_async(args=(current_collection.to_json(),))
    return jsonify({}), 202, {'Location': url_for('tasks.get_status',
                              task_id=task.id)}

