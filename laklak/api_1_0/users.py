from flask import jsonify, abort, url_for, g
from . import api
from .. import db
from ..models import User
from ..errors import forbidden

@api.route('/users', methods=['GET'])
def get_users():
    users = [user.to_json() for user in User.query.all()] # TODO add pagination
    return jsonify({'users':users})

@api.route('/users/<int:user_id>', methods=['GET'])
def get_user(user_id):
    user = User.query.get_or_404(user_id)
    if g.user.id != user.id:
        return forbidden("Insuffient permission")
    return jsonify(user.to_json())

@api.route('/users/<int:user_id>', methods=['DELETE'])
def delete_user(user_id):
    user = User.query.get_or_404(user_id)
    if g.user.id != user.id:
        return forbidden("Insuffient permission")        
    db.session.delete(user)
    db.session.commit()
    return jsonify({'deleted': user_id}), 201
