from flask import jsonify, abort, request, url_for, g, current_app
from . import api
from .. import db
from ..models import Collection, Segment
from ..errors import forbidden
from ..tasks import create_automated_segments
from time import sleep

def get_collection_or_404(collection_id):
    current_collection = g.user.collections.filter_by(id=collection_id).first()
    if not current_collection:
        abort(404, 'collection not found (for the current user)')
    return current_collection

@api.route('/collections/<int:collection_id>/segments', methods=['GET'])
def get_segments(collection_id):
    current_collection = get_collection_or_404(collection_id)
    json_segments= [segment.to_json() for segment in current_collection.segments.all()]
    return jsonify({'segments':json_segments})

@api.route('/collections/<int:collection_id>/create_segments', methods=['POST'])
def create_segments(collection_id):
    current_collection = get_collection_or_404(collection_id)
    task = create_automated_segments.apply_async(args=(current_collection.to_json(),))
    return jsonify({}), 202, {'Location': url_for('tasks.get_status',
                              task_id=task.id)}

@api.route('/collections/<int:collection_id>/segments/<int:segment_id>', \
                                                             methods=['GET'])
def get_segment(collection_id, segment_id):
    current_collection = g.user.collections.filter_by(id=collection_id).first()
    current_segment = Segment.query.get_or_404(segment_id)
    if current_segment.collection_id != collection_id:
        abort(404, 'segment not found (for the current collection)')
    return jsonify(current_segment.to_json())

@api.route('/collections/<int:collection_id>/segments', methods=['POST'])
def create_segment(collection_id):
    current_collection = get_collection_or_404(collection_id)
    if not request.json or not 'start_seconds' in request.json \
                        or not 'end_seconds' in request.json:
        abort(400)
    segment = Segment.from_json(request.json)
    segment.collection_id = collection_id
    db.session.add(segment)
    db.session.commit()
    return jsonify(segment.to_json()), 201, \
           {'Location': url_for('api.get_segment',
                                collection_id=collection_id,
                                segment_id=segment.id,
                                _external=True)}

@api.route('/collections/<int:collection_id>/segments/<int:segment_id>', \
                                                              methods=['PUT'])
def update_segment(collection_id, segment_id):
    current_collection = get_collection_or_404(collection_id)
    segment = Segment.query.get_or_404(segment_id)
    if segment.collection_id != collection_id:
        abort(404, 'segment not found (for the current collection)')
    segment.start_seconds = request.json.get('start_seconds',segment.start_seconds)
    segment.end_seconds = request.json.get('end_seconds',segment.end_seconds)
    segment.transcript = request.json.get('transcript',segment.transcript)
    segment.note = request.json.get('note',segment.note)
    segment.confirmed = request.json.get('confirmed',segment.confirmed)
    db.session.commit()
    return jsonify(segment.to_json()), 201, \
           {'Location': url_for('api.get_segment',
                                collection_id=collection_id,
                                segment_id=segment_id,
                                _external=True)}

@api.route('/collections/<int:collection_id>/segments/<int:segment_id>', \
                                                            methods=['DELETE'])
def delete_segment(collection_id, segment_id):
    current_collection = get_collection_or_404(collection_id)
    segment = Segment.query.get_or_404(segment_id)
    if segment.collection_id != collection_id:
        abort(404, 'segment not found (for the current collection)')
    db.session.delete(segment)
    db.session.commit()
    return jsonify({'deleted': segment_id}), 201 
