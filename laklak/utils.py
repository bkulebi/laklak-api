import os
import subprocess

from lxml import etree
from six.moves.urllib.parse import quote, unquote

download_methods = ['curl','webdav']

class SystemPrequisiteCheck(object):
    def __init__(self):
        self.list_of_av_tools = ['ffmpeg','avconv']
        self.list_of_sys_tools = ['curl']
        self.list_of_env_vars = ['DATA_PATH',
                                 'CLOUD_USER',
                                 'CLOUD_PASSWORD',
                                 'CLOUD_BASE_URL',
                                 'CLOUD_DROPZONE']

    def check_env_vars(self):
        env_vars_exist = os.environ.get(self.list_of_env_vars[0])
        for env_var in self.list_of_env_vars[1:]:
            env_vars_exist = env_vars_exist and os.environ.get(env_var)
        if env_vars_exist: env_vars_exist = True
        return env_vars_exist

    def check_os_tools(self):
        av_tools_exist = False
        for av_tool in self.list_of_av_tools:
            av_tools_exist = av_tools_exist or self.check_tool_exists(av_tool)

        sys_tools_exist = self.check_tool_exists(self.list_of_sys_tools[0])
        sys_tools_exist
        for sys_tool in self.list_of_sys_tools[1:]:
            sys_tools_exist = sys_tools_exist and \
                                                self.check_tool_exists(sys_tool)

        necessary_tools_exist = av_tools_exist and sys_tools_exist
        return necessary_tools_exist 

    def check_tool_exists(self,cmd):
        if type(cmd) != str or len(cmd.split()) > 1:
            raise ValueError('%s is probably not an executable (to check)'%cmd)
        try:
            subprocess.call([cmd, '--help'])
        except:
            return False
        return True

class CollectionResourceDownloader(object):
    def __init__(self, url=None, directory=None, method='curl'):
        self.url = url
        self.target_directory = directory
        self.method = method
        self.cloud_url = os.getenv('CLOUD_BASE_URL')
        relative_dropzone = os.getenv('CLOUD_DROPZONE')
        if relative_dropzone[0]=='/':
            self.dropzone = os.path.join(self.cloud_url,relative_dropzone[1:])
        else:
            self.dropzone = os.path.join(self.cloud_url,os.getenv('CLOUD_DROPZONE'))
        self.cloud_user = os.getenv('CLOUD_USER')
        self.cloud_password = os.getenv('CLOUD_PASSWORD')

    def download_resource(self):
        # TODO url2filename conversion and checks
        self.filename = os.path.basename(self.url)

        methods = ['curl','webdav']
        if self.method not in methods:
            raise ValueError('download method %s not known.'%self.method) 

        if self.method == 'curl':
            self.curl_download()
        elif self.method == 'webdav':
            self.webdav_download()

    def curl_download(self):
        if not self.filename:
            raise ValueError('the url %s seem to be not pointing to a file'\
                             ' (ends with \)'%self.url)

        # check the http headers
        status = self.get_status_code()
        if status == 404:
            raise ValueError('the resource in the given url does not exist.')

        # check if data directory exists if not create one
        if not os.path.isdir(self.target_directory):
            os.makedirs(self.target_directory)
        full_path = os.path.join(self.target_directory,self.filename)

        # create file
        with open(full_path,'w') as fout:
            cmd = ['curl','-g',self.url]
            subprocess.call(cmd,stdout=fout)

    def get_status_code(self):
        cmd = ['curl','-I',self.url]
        header = subprocess.check_output(cmd)

        return int(header.split(b'\n')[0].split()[1])

    def webdav_download(self):
        # the self.url should simply be the file name in webdav
        full_path = os.path.join(self.target_directory,self.filename)

        # check if the given name is in the list dropzone list
        possible_resources = [resource for resource in self.get_dropzone_uris()]
        if self.url not in [unquote(os.path.basename(r)) \
                                                  for r in possible_resources]:
            raise ValueError('resource %s does not exist in the cloud'\
                                                        ' dropzone'%(self.url))
        else:
            cmd = ['curl','-g','-u','%s:%s'%(self.cloud_user,
                                             self.cloud_password),
                                             self.dropzone]
            with open(full_path,'w') as fout:
                subprocess.call(cmd,stdout=fout)

    def get_dropzone_uris(self):
        cmd = ['curl','-g','-u','%s:%s'%(self.cloud_user,self.cloud_password),
                            self.dropzone,'-X','PROPFIND']
        xml = subprocess.check_output(cmd).decode('utf8')
        elements = etree.XML(xml.replace('d:',''))
        return [e.text for e in elements.xpath('//href')]

    def check_dropzone(self):
        cmd = ['curl','-I','-u','%s:%s'%(self.cloud_user,self.cloud_password),
                                                                 self.dropzone]
        header = subprocess.check_output(cmd)
        status_code = int(header.split(b'\n')[0].split()[1])
        if status_code == 200:
            return True
        else:
            return False
