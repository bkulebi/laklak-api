from flask import Blueprint, url_for, abort, jsonify
from celery import states
from time import sleep
import subprocess
import os

from . import celery, db
from .models import Segment, Collection
from .utils import CollectionResourceDownloader

tasks = Blueprint('tasks',__name__)
task_types = ['automated_segments', 'collection_resource']


@celery.task
def create_automated_segments(collection_json, smoothWindow=0.2, \
                                                    Weight=0.1, rewrite=False):
    collection_uri = collection_json.get('uri')
    from .wsgi_aux import app
    # create stub segments
    segments = []
    sleep(2)
    with app.app_context():
        for i in range(10):
            start = i
            end = start+1
            segment_json = {'start_seconds':start,
                             'end_seconds':end}
            segment = Segment.from_json(segment_json)
            segment.collection_id = collection_json.get('id')
            db.session.add(segment)
        db.session.commit()
    # we are passing the collection id to self.AsyncResult.info in order to
    # generate the get_segments url in the get_status, which is a non-async
    # application context
    return {'collection_id': collection_json.get('id'),
            'task_type': task_types[0]}

@celery.task
def download_collection_resource(collection_json):
    from .wsgi_aux import app
    with app.app_context():
        collection = Collection.query.get_or_404(collection_json.get('id'))

        # check if data directory exists if not create one
        directory = os.path.join(os.getenv('DATA_PATH'),str(collection.user_id))
        downloader = CollectionResourceDownloader(collection.uri,directory)

        downloader.download_resource()

        # write to db
        collection.path = downloader.filename
        db.session.commit()
    return {'collection_id': collection_json.get('id'),
            'task_type': task_types[1]}

@tasks.route('/status/<task_id>', methods=['GET'])
def get_status(task_id):
    '''
    Returns the status of the async task. If 202, it hasn't finished yet
    '''
    task = create_automated_segments.AsyncResult(task_id)
    if task.state == states.PENDING:
        abort(404)
    if task.state == states.RECEIVED or task.state == states.STARTED:
        return jsonify({}), 202, {'Location':\
                                  url_for('tasks.get_status', task_id=task_id)}
    # not the most elegant solution to respond to multiple celery tasks.
    # TODO the decorator solution
    task_type_urls = {task_types[0]: 'api.get_segments',
                      task_types[1]: 'api.get_collection'}
    collection_id = task.info['collection_id']
    task_type = task.info['task_type']
    if task_type not in task_types:
        raise ValueError("tasks.get_status did not receive a known task: "\
                         "%s not part of %s"%(task_type,', '.join(task_types)))

    url_result = url_for(task_type_urls[task_type],collection_id=collection_id)
    return jsonify({'state':task.state}), 303, {'Location': url_result}
