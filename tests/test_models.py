import unittest
from laklak import create_app, db
from laklak.models import User, Collection, Segment

TEST_USERS = [{'username': u'Lenin',
               'password': u'1921'},
              {'username': u'Durruti',
               'password': u'1938'}]

TEST_COLLECTIONS = [{'collection_name': 'state and revolution',
                     'uri': 'https://www.marxists.org/',
                     'download_method':'curl'},
                    {'collection_name': '5 year economic plan',
                     'uri': 'https://www.marxists.org/',
                     'download_method':'webdav'}]

TEST_SEGMENTS = [{'start_seconds':'0', 
                  'end_seconds':'9.3',
                  'transcript':'',
                  'note':''},
                 {'start_seconds':'9.3', 
                  'end_seconds':'12.4',
                  'transcript':'',
                  'note':'ignore'}]

def transpose(data):
        return [[data[j][i] for j in range(len(data))] for i in range(len(data[0]))]

class SegmentModelTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()

        db.create_all()
        self.test_user_json = {'username': u'Marcos',
                               'password': '1994'}
        user = User.from_json(self.test_user_json)
        db.session.add(user)
        db.session.commit()

        self.test_collection_json = {'collection_name':u'La Historia de los Colores'}
        self.collection = Collection.from_json(self.test_collection_json)
        self.collection.user_id = user.id
        db.session.add(self.collection)
        db.session.commit()

        for test_segment in TEST_SEGMENTS:
            segment = Segment.from_json(test_segment)
            segment.collection_id = self.collection.id
            db.session.add(segment)
        db.session.commit()

        segment_field_ref = [(float(segment['start_seconds']),\
                             float(segment['end_seconds']),\
                             segment.get('transcript'),\
                             segment.get('note')) for segment in TEST_SEGMENTS]
        self.segment_columns_ref = transpose(segment_field_ref)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_create(self):
        # test direct initialization
        first_segment = Segment(start_seconds=0.0,
                                end_seconds=3.2,
                                transcript=u'Selamun aleykum beser',
                                note='speech',
                                confirmed=False)

        segments_in_db = Segment.query.filter_by(
                                        collection_id=self.collection.id).all()
        # direct access to db
        self.assertEqual(len(segments_in_db),len(TEST_SEGMENTS))

        # from to_json method
        segment_fields_in_db = [(segment.start_seconds,
                                segment.end_seconds,
                                segment.transcript,
                                segment.note) for segment in segments_in_db]

        segment_columns_in_db = transpose(segment_fields_in_db)

        for i, segment_column_ref in enumerate(self.segment_columns_ref):
            self.assertEqual(set(segment_column_ref),set(segment_columns_in_db[i]))

    def test_relationship(self):
        test_collection = Collection.query.filter_by(collection_name=\
                          self.test_collection_json['collection_name']).first()
        test_collection_segments = test_collection.segments.all()

        segment_fields_in_db = [(segment.start_seconds,
                                segment.end_seconds,
                                segment.transcript,
                                segment.note)\
                                for segment in test_collection_segments]
 
        segment_columns_in_db = transpose(segment_fields_in_db)

        for i, segment_column_ref in enumerate(self.segment_columns_ref):
            self.assertEqual(set(segment_column_ref),set(segment_columns_in_db[i]))

class CollectionModelTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()

        db.create_all()
        self.test_user_json = {'username': u'Trotsky',
                               'password': '1918'}
        self.user = User.from_json(self.test_user_json)
        db.session.add(self.user)
        db.session.commit()
        for test_collection in TEST_COLLECTIONS:
            collection = Collection.from_json(test_collection)
            collection.user_id = self.user.id
            db.session.add(collection)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_create(self):
        # test direct initialization
        for test_collection in TEST_COLLECTIONS:
            collection_in_db = Collection.query.filter_by(
                     collection_name=test_collection['collection_name']).first()

            # direct access to db
            self.assertEqual(collection_in_db.collection_name,
                             test_collection['collection_name'])
            self.assertEqual(collection_in_db.uri,
                             test_collection['uri'])
            self.assertEqual(collection_in_db.download_method,
                             test_collection['download_method'])

            # from to_json method
            collection_in_db_json = collection_in_db.to_json()
            self.assertEqual(collection_in_db_json.get('collection_name'),
                             test_collection['collection_name'])
            self.assertEqual(collection_in_db_json.get('uri'),
                             test_collection['uri'])
            self.assertEqual(collection_in_db_json.get('download_method'),
                             test_collection['download_method'])


        # test to_json fields
        to_json_fields = ['url', 'id', 'collection_name', 'username', 
                          'uri', 'download_method', 'path']
        self.assertEqual(set(collection_in_db_json.keys()),set(to_json_fields))

    def test_relationship(self):
        test_user = User.query.filter_by(
                              username=self.test_user_json['username']).first()
        test_user_collections = test_user.collections.all()

        test_collection_names_reference = [collection['collection_name'] \
                                           for collection in TEST_COLLECTIONS]
        test_collection_names_in_db = [collection.collection_name \
                                        for collection in test_user_collections]
        self.assertEqual(set(test_collection_names_in_db),
                         set(test_collection_names_reference))

class UserModelTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()

        db.create_all() 
        for test_user in TEST_USERS:
            user = User.from_json(test_user)
            db.session.add(user)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_create(self):
        # test direct initialization
        first_user = User(username='Roza',
                          password='1921')

        # query users in db
        for test_user in TEST_USERS:
            user_in_db = User.query.filter_by(username=test_user['username']).first()

            # direct access to db
            self.assertEqual(user_in_db.username,test_user['username'])

            # from to_json method
            user_in_db_json = user_in_db.to_json()
            self.assertEqual(user_in_db_json.get('username'),test_user['username'])

            # test password creation
            with self.assertRaises(AttributeError):
                user_in_db.password

            self.assertIs(type(user_in_db.password_hash),str)

            # verify password
            test_password = [test_user['password'] for test_user in TEST_USERS \
                            if test_user['username'] == user_in_db.username]
            self.assertTrue(user_in_db.verify_password(test_password[0]))

            # check url 
            try:
                self.assertTrue('/laklak/' in user_in_db_json['url'])
            except:
                print('url not correct %s:'%user_in_db_json['url'])
                raise

        # test to_json fields 
        to_json_fields = ['url', 'username', 'id']
        self.assertEqual(set(user_in_db_json.keys()),set(to_json_fields)) 
