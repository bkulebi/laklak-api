import unittest
import logging
import json
import os
from time import sleep
from flask import url_for
from laklak import create_app, db
from laklak.models import User, Collection

from test_client import TestClient

class APITestCase(unittest.TestCase):
    default_username = 'lenin'
    default_password = '1919'

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.drop_all()
        db.create_all()

        u1 = User(username=self.default_username,
                 password=self.default_password)
        u2 = User(username='durruti',
                 password='1938')
        db.session.add(u1)
        db.session.add(u2)
        db.session.commit()

        self.client = TestClient(self.app)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def get_headers(self):
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'}
        return headers

    def get_result_from_task(self, task_url, token):
        results_url = None
        in_queue = True
        retry = 0
        while in_queue:
            rv, json_response = self.client.get(task_url,
                                                token_auth=token)
            retry += 1
            if rv.status_code != 303:
                # finished and given a redirect
                logging.info(rv.status) # TODO logging
                sleep(3)
                self.assertTrue(retry < 5)
            else:
                results_url = rv.headers['Location'] 
                in_queue = False
        return results_url

    def test_404(self):
        rv, json_response = self.client.get('/wrong/url')
        #json_response = json.loads(rv.data.decode('utf-8'))

        self.assertEqual(rv.status_code,404)
        self.assertEqual(type(json_response),dict)

    def test_api(self):
        # get users with bad auth
        rv, json_response = self.client.get(url_for('api.get_users'),
                                            token_auth='bad-token')
        self.assertEqual(rv.status_code,401)

        # request a token
        rv, json_response = self.client.post(url_for('token.request_token'),
                                             basic_auth='lenin:1919')
        self.assertEqual(rv.status_code,200)
        token = json_response['token']

        # request a token with wrong password
        rv, json_response = self.client.post(url_for('token.request_token'),
                                             basic_auth='lenin:1905')
        self.assertEqual(rv.status_code,401)

        # get user list
        rv, json_response = self.client.get(url_for('api.get_users'),
                                            token_auth=token)
        self.assertEquals(rv.status_code,200)

        for user in json_response['users']:
            username = user['username']
            if username == self.default_username:
                user_id = user['id']
            else:
                otheruser_id = user['id']
                otheruser_username = user['username']
        self.assertTrue(username,self.default_username)

        # get user
        rv, json_response = self.client.get(url_for('api.get_user',
                                                    user_id=user_id,
                                                    _external=True),
                                            token_auth=token)
        self.assertEquals(rv.status_code,200)

        # get other user
        rv, json_response = self.client.get(url_for('api.get_user',
                                                    user_id=otheruser_id,
                                                    _external=True),
                                            token_auth=token)
        self.assertEquals(rv.status_code,403)

        # update user password

        # create collection
        test_collection = {'collection_name':'state and revolution',
                           'uri':'https://www.marxists.org/'}
        rv, json_response = self.client.post(url_for('api.create_collection'),
                                            data=test_collection,
                                            token_auth=token)
        self.assertEquals(rv.status_code,201)
        self.assertEquals(json_response['collection_name'],
                          test_collection['collection_name'])
        self.assertEquals(json_response['uri'],
                          test_collection['uri'])

        # get collection list
        rv, json_response = self.client.get(url_for('api.get_collections'),
                                            token_auth=token)
        self.assertEquals(rv.status_code,200)
        test_collection_id = json_response['collections'][0]['id']

        # get collection
        rv, json_response = self.client.get(url_for('api.get_collection',
                                             collection_id=test_collection_id),
                                            token_auth=token)
        self.assertEquals(rv.status_code,200)

        # update collection
        test_update_collection_name = {'collection_name':'what needs to be done?'}
        test_update_uri = {'uri':'https://httpbin.org/xml'}
        rv, json_response = self.client.put(url_for('api.update_collection',
                                            collection_id=test_collection_id),
                                            data=test_update_uri,
                                            token_auth=token)
        self.assertEquals(rv.status_code,201)

        # check if uri is updated AND collection_name is still the same
        self.assertEquals(json_response['collection_name'],
                          test_collection['collection_name'])
        self.assertEquals(json_response['uri'],test_update_uri['uri'])

        # update only the collection_name
        rv, json_response = self.client.put(url_for('api.update_collection',
                                            collection_id=test_collection_id),
                                            data=test_update_collection_name,
                                            token_auth=token)
        self.assertEquals(rv.status_code,201)

        # check if collection_name is updated and uri is still the same
        self.assertEquals(json_response['collection_name'],
                          test_update_collection_name['collection_name'])
        self.assertEquals(json_response['uri'],test_update_uri['uri'])

        # download collection resource (audio file) from uri
        rv, json_response = self.client.post(
                                      url_for('api.create_collection_resource',
                                      collection_id=test_collection_id),
                                      token_auth=token)
        self.assertEquals(rv.status_code,202)
        task_url = rv.headers['Location']
        results_url = self.get_result_from_task(task_url,token)

        self.assertEquals(url_for('api.get_collection',
                                 collection_id=test_collection_id),results_url)

        # check if resouce downloaded
        rv, json_response = self.client.get(
                                        url_for('api.get_collection',
                                        collection_id=test_collection_id),
                                        token_auth=token)
        self.assertEquals(rv.status_code,200)
        self.assertNotEquals(json_response['path'],None)

        # check if file actually "downloaded"
        current_collection = Collection.query.get_or_404(test_collection_id)
        file_path = os.path.join(os.getenv('DATA_PATH'),
                                 str(current_collection.user_id),
                                 json_response['path'])
        self.assertTrue(os.path.exists(file_path))
        # clean the created file TODO move to teardown
        os.popen('rm %s'%file_path)

        # test webdav download
        webdav_collection = {'collection_name':'bullshit',
                             'download_method':'webdav'}
        rv, json_response = self.client.post(url_for('api.create_collection'),
                                             data=webdav_collection,
                                             token_auth=token)
        self.assertEquals(rv.status_code,201)

        self.assertEqual(json_response['download_method'],
                         webdav_collection['download_method'])

        # create segment
        rv, json_response = self.client.post(url_for('api.create_segment',
                                             collection_id=test_collection_id),
                                             data = {'start_seconds':'0.5',
                                                     'end_seconds':'5.5',
                                                     'note':'speech'},
                                             token_auth=token)
        self.assertEquals(rv.status_code,201)
        self.assertEquals(json_response['note'],'speech')

        # get segment
        test_segment_json = json_response
        rv, json_response = self.client.get(url_for('api.get_segment',
                                           collection_id=test_collection_id,
                                           segment_id=test_segment_json['id']),
                                            token_auth=token)
        self.assertEquals(rv.status_code,200)

        # create async segments
        rv, json_response = self.client.post(url_for('api.create_segments',
                                            collection_id=test_collection_id),
                                            token_auth=token)
        self.assertEquals(rv.status_code,202)
        task_url = rv.headers['Location']
        results_url = self.get_result_from_task(task_url, token)
        self.assertTrue(url_for('api.get_segments',
                              collection_id=test_collection_id) in results_url)

        # get segments
        rv, json_response = self.client.get(url_for('api.get_segments',
                                             collection_id=test_collection_id),
                                            token_auth=token)
        self.assertEquals(rv.status_code,200)
        self.assertTrue(len(json_response['segments'])>1)

        # update segment
        rv, json_response = self.client.put(test_segment_json['url'],
                                            data={'transcript':'testing',
                                                  'confirmed':'True'},
                                            token_auth=token)
        self.assertEquals(rv.status_code,201)
        self.assertEquals(json_response['transcript'],'testing')
        self.assertEquals(json_response['confirmed'],True)

        # delete segment
        rv, json_response = self.client.delete(url_for('api.delete_segment',
                                           collection_id=test_collection_id,
                                           segment_id=test_segment_json['id']),
                                            token_auth=token)
        self.assertEquals(rv.status_code,201)

        # delete collection
        rv, json_response = self.client.delete(url_for('api.delete_collection',
                                          collection_id=test_collection_id),
                                            token_auth=token)
        self.assertEquals(rv.status_code,201)

        # delete user?
        rv, json_response = self.client.delete(url_for('api.delete_user',
                                                              user_id=user_id),
                                            token_auth=token)
        self.assertEquals(rv.status_code,201)
