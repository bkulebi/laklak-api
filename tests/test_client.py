from base64 import b64encode
from werkzeug.exceptions import HTTPException
import json

class TestClient():
    def __init__(self, app):
        '''basic_auth: "username:password" 
           token_auth: token'''
        self.app = app

    def send(self, url, method='GET', data=None,
             basic_auth=None, token_auth=None, headers={},
             content_type='application/json'):
        # Flask's client prefers relative URLs
        url = url.replace('http://localhost', '')

        # assemble the final header list
        headers = headers.copy()
        if basic_auth is not None:
            headers['Authorization'] = 'Basic ' + b64encode(
                                    basic_auth.encode('utf-8')).decode('utf-8')
        if token_auth is not None:
            headers['Authorization'] = 'Basic '+ b64encode(
                                    token_auth.encode('utf-8')).decode('utf-8')

        if 'Content-Type' not in headers:
            headers['Content-Type'] = content_type
        if 'Accept' not in headers:
            headers['Accept'] = content_type

        # generate a body if needed
        if data:
            data = json.dumps(data)

        # send the request
        with self.app.test_request_context(url, method=method, data=data,
                                           headers=headers):
            try:
                rv = self.app.preprocess_request()
                if rv is None:
                    rv = self.app.dispatch_request()
                rv = self.app.make_response(rv)
                rv = self.app.process_response(rv)
            except HTTPException as e:
                rv = self.app.handle_user_exception(e)

        return rv, json.loads(rv.data.decode('utf-8'))

    def get(self, url, basic_auth=None, token_auth=None, headers={}):
        return self.send(url, 'GET', basic_auth=basic_auth, token_auth=token_auth, headers=headers)

    def post(self, url, data=None, basic_auth=None, token_auth=None,
                                                                   headers={}):
        return self.send(url, 'POST', data=data, basic_auth=basic_auth, token_auth=token_auth,
                         headers=headers)

    def put(self, url, data=None, basic_auth=None, token_auth=None,
                                                                   headers={}):
        return self.send(url, 'PUT', data=data, basic_auth=basic_auth, token_auth=token_auth,
                         headers=headers)

    def delete(self, url, basic_auth=None, token_auth=None, headers={}):
        return self.send(url, 'DELETE', basic_auth, token_auth, 
                         headers=headers)
