import unittest
import os
from laklak import utils
from six.moves.urllib.parse import quote, unquote

class CheckPrequisiteTestCase(unittest.TestCase):
    def setUp(self):
        self.sys_check = utils.SystemPrequisiteCheck()

    def tearDown(self):
        pass

    def test_check(self):
        # test the "system tools check"
        os_tools_exist = self.sys_check.check_os_tools()
        self.assertTrue(os_tools_exist)

        # test the "environment variables check"
        env_vars_exists = self.sys_check.check_env_vars()
        self.assertTrue(env_vars_exists)

class CollectionResourceDownloadTestCase(unittest.TestCase):
    def setUp(self):
        #self.url = 'https://httpbin.org/xml'
        self.url = 'https://abs.twimg.com/b/front_page/v1/AR_ES_MEX_LATAM_1.jpg'
        self.data_path = os.getenv('DATA_PATH')
        self.CRD = utils.CollectionResourceDownloader(url=self.url,
                                                 directory=self.data_path,
                                                 method='curl')

    def tearDown(self):
        pass

    def test_fail(self):
        wrong_url1 = 'https://www.marxists.org/'
        self.CRD.url = wrong_url1

        # does not accept a url with / in the end
        with self.assertRaises(ValueError):
            self.CRD.download_resource()

        wrong_url2 = os.path.join(wrong_url1,'lenin.mp3')
        self.CRD.url = wrong_url2

        with self.assertRaises(ValueError):
            self.CRD.download_resource()

    def test_curl_create(self):
        # download resource
        self.CRD.download_resource()

        full_resource_path = os.path.join(self.data_path,self.CRD.filename)
        self.assertTrue(os.path.exists(full_resource_path))
        self.assertNotEquals(os.path.getsize(full_resource_path),0)
        os.remove(full_resource_path)

    def test_webdav_create(self):
        dropzone = os.getenv('CLOUD_DROPZONE')
        self.CRD.method = 'webdav'

        # assert dropzone is a relative adress
        self.assertEquals(dropzone[0],'/')

        # assert dropzone is a 'directory' (ends with /)
        self.assertEquals(os.path.basename(dropzone),'')

        # check if the drop zone parameters are correct
        dropzone_accessible = self.CRD.check_dropzone()
        self.assertTrue(dropzone_accessible)

        # check drop zone listing
        dropzone_uris = self.CRD.get_dropzone_uris()
        self.assertEquals(unquote(dropzone_uris[0]),dropzone)

        if len(dropzone_uris)>1:
            # check wrong uri
            self.CRD.url = os.path.basename(unquote(dropzone_uris[1])[:-2])
            with self.assertRaises(ValueError):
               self.CRD.download_resource()

            # check downloading
            self.CRD.url = os.path.basename(unquote(dropzone_uris[1]))
            self.CRD.download_resource()
            full_resource_path = os.path.join(self.data_path,self.CRD.filename)
            self.assertTrue(os.path.exists(full_resource_path))
            self.assertNotEquals(os.path.getsize(full_resource_path),0)
            os.remove(full_resource_path)
