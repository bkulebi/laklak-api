# LakLak RESTful API

## run program
```
celery worker -A laklak.tasks.celery --loglevel=info
python manage.py runserver
```

## tests
for specific test modules
```
python3 -m unittest discover -s tests -p "test_<module>.py" -v
```
